//
//  MeteorsMyWallStUITests.swift
//  MeteorsMyWallStUITests
//
//  Created by Javier Servate on 25/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import XCTest

class MeteorsMyWallStUITests: XCTestCase {
    
    var testApp = XCUIApplication()
    
    override func setUp() {
        testApp.launch()
    }
    
    //This test must be done standalone and only when the app is fresh installed. Otherwise it won't work as it tests if the alert shows up when it cannot load remote data on the first run
    func testShowsAlertInFirstRun() {
        testApp.terminate()
        
        testApp.launchArguments += ["-isFirstRun", "true"]
        
        //In case the test is being done on a real device, uncomment these lines. Otherwise, if it is being done on a virtual device just leave them uncommented and disconnect the wifi on the Macbook
//        let settingsApp = XCUIApplication(bundleIdentifier: "com.apple.Preferences")
//        settingsApp.launch()
//        settingsApp.tables.cells["Airplane Mode"].tap()
        
        testApp.launch()
        XCTAssertTrue(testApp.alerts["Error"].waitForExistence(timeout: 5))
    }
    
    func testSwitchToDetailsView() {
        let mapView = testApp.maps.element
        testApp.cells.element(boundBy: 1).tap()
        XCTAssertTrue(mapView.waitForExistence(timeout: 1))
    }
    
    func testLongPressOnTableViewCell() {
        let mapView = testApp.maps.element
        testApp.cells.element(boundBy: 1).press(forDuration: 5)
        XCTAssertTrue(mapView.waitForExistence(timeout: 2))
    }
    
    func testSwitchToWebView() {
        let webView = testApp.webViews.element
        testApp.cells.element(boundBy: 1).tap()
        testApp.buttons["More information"].tap()
        XCTAssertTrue(webView.waitForExistence(timeout: 10))
    }
    
    func testSwitchBackToDetailView() {
        let mapView = testApp.maps.element
        testApp.cells.element(boundBy: 1).tap()
        testApp.buttons["More information"].tap()
        testApp.navigationBars.buttons.element(boundBy: 0).tap()
        XCTAssertTrue(mapView.waitForExistence(timeout: 2))
    }
    
    func testSwitchBackToListView() {
        let tableView = testApp.tables.element
        testApp.cells.element(boundBy: 1).tap()
        testApp.navigationBars.buttons.element(boundBy: 0).tap()
        XCTAssertTrue(tableView.waitForExistence(timeout: 1))
    }
    
    func testHasSearchBar() {
        XCTAssertNotNil(testApp.searchFields.element)
    }
    
    func testSearchAllowsEditing() {
        let searchBar = testApp.searchFields.element
        searchBar.tap()
        testApp.searchFields.element.typeText("Searching a specific meteor")
        XCTAssertEqual(searchBar.value as? String, "Searching a specific meteor")
    }
    
    func testToggleToHideStackViewFromDetailsView() {
        let mapView = testApp.maps.element
        testApp.cells.element(boundBy: 1).tap()
        testApp.buttons["Toggle Stack"].tap()
        XCTAssertTrue(!testApp.staticTexts["Classification: CR2"].waitForExistence(timeout: 1))
    }
    
    func testToggleToShowStackViewFromDetailsView() {
        let mapView = testApp.maps.element
        testApp.cells.element(boundBy: 1).tap()
        testApp.buttons["Toggle Stack"].tap()
        //Wait until the animation finishes. Using sleep in a UITest does not affect as it runs in another process
        sleep(1)
        testApp.buttons["Toggle Stack"].tap()
        XCTAssertTrue(testApp.staticTexts["Classification: CR2"].waitForExistence(timeout: 1))
    }

    override func tearDown() {

    }

}
