# MeteorsMyWallSt

**Displays a list of the fallen meteors on the Earth since 2011 using the NASA Open Data API. The list has a search functionality as well, and when tapped any cell each meteor has its own details view displaying a map (MapKit iOS framework) with a location pin related to its coordinates along with some relevant information and details. The user can learn more about it opening a WebView within the app that will show a Google search about the specific meteor selected.**

On the first run it will retrieve the information from the server and it will store it locally using a Realm database. In the next runs it will check for any updates of the API using the header from the HTTP response and it case there is any, it will update it using a background thread.

**Designed, implemented and structured always following the SOLID principles to make it as much reusable, scalable and easy to maintain as possible.**




