//
//  Meteor.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import Foundation
import RealmSwift

typealias Meteors = [Meteor]

class Meteor: Object, Codable {
    let fall: Fall
    let geolocation: Geolocation
    @objc dynamic var id, mass, name: String
    let nametype: Nametype
    @objc dynamic var recclass, reclat, reclong: String
    @objc dynamic var year: String
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

enum Fall: String, Codable {
    case fell = "Fell"
    case found = "Found"
}

struct Geolocation: Codable {
    let type: TypeEnum
    let coordinates: [Double]
}

enum TypeEnum: String, Codable {
    case point = "Point"
}

enum Nametype: String, Codable {
    case relict = "Relict"
    case valid = "Valid"
}
