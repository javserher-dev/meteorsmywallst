//
//  Constants.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

typealias AlertActionCompletion = (UIAlertAction) -> Void

struct Constants {
    
    struct APIPaths {
        static let baseUrl = "https://data.nasa.gov/resource"
        static let landings = "/y77d-th95.json"
    }
    
    struct APIResponseParameters {
        static let lastModified = "Last-Modified"
        static let dateFormat = "E, d MMM yyyy HH:mm:ss zzz"
    }
    
     static let jsonDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    
    struct Cell {
        static let cellIdentifier = "Cell"
        static let loadingCellIdentifier = "LoadingCell"
        static let meteorCellNib = "MeteorTableViewCell"
    }
    
    struct UserDefaultsKeys {
        static let isFirstRun = "isFirstRun"
        static let lastRequest = "lastRequest"
    }
    
    struct NotificationNames {
        static let appBecameActive = "appBecameActive"
    }
    
    static let googleSearchUrl = "https://www.google.com/search?q="
    
    static let meteorImages = [Asset.meteor1, Asset.meteor2, Asset.meteor3, Asset.meteor4, Asset.meteor5, Asset.meteor6]
}
