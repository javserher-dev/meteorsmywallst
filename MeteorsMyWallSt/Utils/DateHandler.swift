//
//  DateHandler.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 24/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

enum DateConversionMode {
    case spanishWeekday, englishWeekday, day, month, year, time, hour, minutes, seconds, dayTime, monthTime, yearTime, dayMonth, monthYear
}

class DateHandler: NSObject {
    
    let spanishWeekdays = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"]
    let englishWeekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    
    func convertToString (date: Date, format: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: date)
    }
    
    func convertToDate (dateString: String, format: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.date(from: dateString)
    }
    
    func getDateSpecificValue (date: Date, mode: DateConversionMode) -> String {
        let values = Calendar.current.dateComponents([.weekday, .day, .month, .year, .hour, .minute, .second], from: date)
        
        switch mode {
            
        case .spanishWeekday:
            guard let weekday = values.weekday else { return "" }
            return spanishWeekdays[weekday - 1]
        case .englishWeekday:
            guard let weekday = values.weekday else { return "" }
            return englishWeekdays[weekday - 1]
        case .day:
            guard let day = values.day else { return "" }
            return String (day)
        case .month:
            guard let month = values.month else { return "" }
            return String (month)
        case .year:
            guard let year = values.year else { return "" }
            return String (year)
        case .time:
            guard let hour = values.hour, let minutes = values.minute, let seconds = values.second else { return "" }
            return String(format: "%0.2d:%0.2d:%0.2d", hour, minutes, seconds)
        case .hour:
            guard let hour = values.hour else { return "" }
            return String (hour)
        case .minutes:
            guard let minutes = values.minute else { return "" }
            return String (minutes)
        case .seconds:
            guard let seconds = values.second else { return "" }
            return String (seconds)
        case .dayTime:
            guard let day = values.day, let hour = values.hour, let minutes = values.minute, let seconds = values.second else { return "" }
            return String(format: "%d %0.2d:%0.2d:%0.2d", day, hour, minutes, seconds)
        case .monthTime:
            guard let month = values.month, let hour = values.hour, let minutes = values.minute, let seconds = values.second else { return "" }
            return String(format: "%d %0.2d:%0.2d:%0.2d", month, hour, minutes, seconds)
        case .yearTime:
            guard let year = values.year, let hour = values.hour, let minutes = values.minute, let seconds = values.second else { return "" }
            return String(format: "%d %0.2d:%0.2d:%0.2d", year, hour, minutes, seconds)
        case .dayMonth:
            guard let day = values.day, let month = values.month else { return "" }
            return String(format: "%d/%d", day, month)
        case .monthYear:
            guard let month = values.month, let year = values.year else { return "" }
            return String(format: "%d/%d", month, year)
        }
    }
    
}
