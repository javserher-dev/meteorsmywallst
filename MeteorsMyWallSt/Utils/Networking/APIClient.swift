//
//  APIClient.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import Alamofire

final class APIClient: NSObject {
    
    static let shared = APIClient()
    
    func performRequest<T: Decodable>(router: APIRouter, completion: @escaping (_: T?) -> Void) {
        Alamofire.request(router).responseData(completionHandler: { [weak self] result in
            guard let response = result.response, let data = result.data, (200...300).contains(response.statusCode) else {
                debugPrint("Request did not succeed")
                completion(nil)
                return
            }
            completion(try? JSONDecoder().decode(T.self, from: data))
        })
    }
    
    func loadResponse(router: APIRouter, completion: @escaping (_: HTTPURLResponse?) -> Void) {
        Alamofire.request(router).responseData(completionHandler: { [weak self] result in
           completion(result.response)
        })
    }
}
