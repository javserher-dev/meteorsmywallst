//
//  APIRouter.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import Alamofire

enum APIRouter: URLRequestConvertible {
    case landings, checkUpdates
    
    var path: String {
        switch self {
        case .landings, .checkUpdates: return Constants.APIPaths.landings
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .landings, .checkUpdates: return .get
        }
    }
    
    var header: HTTPHeaders {
        switch self {
        case .landings, .checkUpdates: return ["X-App-Token": "12DWEvQWF8zTqSn6K8bxOLIN2", "Accept": "application/json"]
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .landings: return ["$where": "year between '2011-01-10T12:00:00' and '2019-01-10T14:00:00'", "$order": "mass"]
        case .checkUpdates: return ["$limit": 0]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.APIPaths.baseUrl.asURL()
        var request = URLRequest(url: url.appendingPathComponent(path))
        request.allHTTPHeaderFields = header
        request.httpMethod = method.rawValue
        switch method {
        case .get: return try URLEncoding().encode(request, with: parameters)
        default: return try JSONEncoding().encode(request, with: parameters)
        }
    }
}
