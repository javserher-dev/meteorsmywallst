//
//  DBManager.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import RealmSwift

final class DBManager: NSObject {
    
    static let shared = DBManager()
    
    func save<T: Object>(data: [T]) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                for info in data {
                    realm.add(info, update: true)
                }
            }
            return true
        } catch {
            debugPrint(error.localizedDescription)
            return false
        }
    }
    
    func load<T: Object>(type: T.Type) -> Results<T>? {
        do {
            let realm = try Realm()
            return realm.objects(type)
        } catch {
            debugPrint(error.localizedDescription)
            return nil
        }
    }
}
