//
//  GoogleSearchQueryFormatter.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class GoogleSearchQueryFormatter: NSObject {
    
    func formatToGoogleQuery(text: String) -> String {
        return Constants.googleSearchUrl + text.replacingOccurrences(of: " ", with: "+")
    }
}
