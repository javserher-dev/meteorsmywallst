//
//  CountryFormatter.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 24/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

class CountryFormatter: NSObject {
    
    func getNameFrom(iso: String) -> String {
        let locale = Locale(identifier: "en_US")
        return locale.localizedString(forRegionCode: iso)!
    }
    
    func getIsoFrom(name: String) -> String {
        let locale = Locale(identifier: "en_US")
        return locale.localizedString(forRegionCode: name)!
    }
}
