// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum Alerts {
    internal enum Buttons {
      /// Cancel
      internal static let cancel = L10n.tr("Localizable", "Alerts.Buttons.cancel")
      /// Done
      internal static let done = L10n.tr("Localizable", "Alerts.Buttons.done")
      /// Ok
      internal static let ok = L10n.tr("Localizable", "Alerts.Buttons.ok")
    }
    internal enum Message {
      /// Could not load information from the server.\n\nRetry?
      internal static let remoteLoadError = L10n.tr("Localizable", "Alerts.Message.remoteLoadError")
    }
    internal enum Titles {
      /// Error
      internal static let error = L10n.tr("Localizable", "Alerts.Titles.error")
    }
  }

  internal enum BlurredLoadingScreen {
    internal enum Labels {
      /// Please wait while loading
      internal static let loading = L10n.tr("Localizable", "BlurredLoadingScreen.Labels.loading")
    }
  }

  internal enum MeteorDetails {
    internal enum Labels {
      /// Classification:
      internal static let classification = L10n.tr("Localizable", "MeteorDetails.Labels.classification")
      /// Country:
      internal static let country = L10n.tr("Localizable", "MeteorDetails.Labels.country")
      /// Country not found
      internal static let countryNotFound = L10n.tr("Localizable", "MeteorDetails.Labels.countryNotFound")
      /// Latitude:
      internal static let latitude = L10n.tr("Localizable", "MeteorDetails.Labels.latitude")
      /// Longitude:
      internal static let longitude = L10n.tr("Localizable", "MeteorDetails.Labels.longitude")
      /// gr
      internal static let massUnit = L10n.tr("Localizable", "MeteorDetails.Labels.massUnit")
    }
  }

  internal enum MeteorList {
    internal enum Placeholders {
      /// Search
      internal static let searchBar = L10n.tr("Localizable", "MeteorList.Placeholders.searchBar")
    }
  }

  internal enum NavigationBar {
    internal enum Titles {
      /// Meteors
      internal static let meteorList = L10n.tr("Localizable", "NavigationBar.Titles.meteorList")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
