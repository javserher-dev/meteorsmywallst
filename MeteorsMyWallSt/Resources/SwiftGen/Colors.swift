// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  internal typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  internal typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal struct ColorName {
  internal let rgbaValue: UInt32
  internal var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffffff"></span>
  /// Alpha: 60% <br/> (0xffffff99)
  internal static let alphaWhite = ColorName(rgbaValue: 0xffffff99)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#000000"></span>
  /// Alpha: 58% <br/> (0x00000094)
  internal static let blackTransparent = ColorName(rgbaValue: 0x00000094)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#0084cb"></span>
  /// Alpha: 100% <br/> (0x0084cbff)
  internal static let blueLine = ColorName(rgbaValue: 0x0084cbff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#d0021b"></span>
  /// Alpha: 100% <br/> (0xd0021bff)
  internal static let crimsonRed = ColorName(rgbaValue: 0xd0021bff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#9b9b9b"></span>
  /// Alpha: 100% <br/> (0x9b9b9bff)
  internal static let grayButton = ColorName(rgbaValue: 0x9b9b9bff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f8f8f8"></span>
  /// Alpha: 100% <br/> (0xf8f8f8ff)
  internal static let grayCell = ColorName(rgbaValue: 0xf8f8f8ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#747578"></span>
  /// Alpha: 100% <br/> (0x747578ff)
  internal static let grayDialog = ColorName(rgbaValue: 0x747578ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#99999b"></span>
  /// Alpha: 20% <br/> (0x99999b33)
  internal static let grayLine = ColorName(rgbaValue: 0x99999b33)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4e5054"></span>
  /// Alpha: 100% <br/> (0x4e5054ff)
  internal static let grayText = ColorName(rgbaValue: 0x4e5054ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#333333"></span>
  /// Alpha: 100% <br/> (0x333333ff)
  internal static let mainblack = ColorName(rgbaValue: 0x333333ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#0084cb"></span>
  /// Alpha: 100% <br/> (0x0084cbff)
  internal static let mainblue = ColorName(rgbaValue: 0x0084cbff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#03437a"></span>
  /// Alpha: 100% <br/> (0x03437aff)
  internal static let maindarkblue = ColorName(rgbaValue: 0x03437aff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#00a933"></span>
  /// Alpha: 100% <br/> (0x00a933ff)
  internal static let maingreen = ColorName(rgbaValue: 0x00a933ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ef8c1e"></span>
  /// Alpha: 100% <br/> (0xef8c1eff)
  internal static let mainorange = ColorName(rgbaValue: 0xef8c1eff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffffff"></span>
  /// Alpha: 100% <br/> (0xffffffff)
  internal static let white = ColorName(rgbaValue: 0xffffffff)
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

// swiftlint:disable operator_usage_whitespace
internal extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}
// swiftlint:enable operator_usage_whitespace

internal extension Color {
  convenience init(named color: ColorName) {
    self.init(rgbaValue: color.rgbaValue)
  }
}
