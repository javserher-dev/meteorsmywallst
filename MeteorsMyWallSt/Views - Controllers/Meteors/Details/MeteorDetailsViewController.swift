//
//  MeteorDetailsViewController.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import MapKit

final class MeteorDetailsViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var meteorNameLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var toggleStackButton: UIButton!
    @IBOutlet weak var detailsStackView: UIStackView!
    @IBOutlet weak var dropdownArrow: UIImageView!
    @IBOutlet weak var classificationLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var classificationDotImageView: UIImageView!
    @IBOutlet weak var latitudeDotImageView: UIImageView!
    @IBOutlet weak var lontigudeDotImageView: UIImageView!
    @IBOutlet weak var countryDotImageView: UIImageView!
    
    var meteor: Meteor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toggleStackButton.accessibilityLabel = "Toggle Stack"
        
        setStyle()
        setContent()
    }
    
    private func setStyle() {
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.backIndicatorImage = Asset.backArrow.image
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = Asset.backArrow.image
        navigationController?.navigationBar.topItem?.title = ""
        
        searchButton.layer.masksToBounds = false
        searchButton.layer.cornerRadius = 5
        searchButton.layer.shadowColor = UIColor.darkGray.cgColor
        searchButton.layer.shadowOpacity = 0.85
        searchButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        searchButton.layer.shadowRadius = 10
        
        createDotIcon(imageView: classificationDotImageView)
        createDotIcon(imageView: latitudeDotImageView)
        createDotIcon(imageView: lontigudeDotImageView)
        createDotIcon(imageView: countryDotImageView)
    }
    
    private func setContent() {
        guard let meteor = meteor else { return }
        meteorNameLabel.text = meteor.name
        
        let latitude = Double(meteor.reclat)
        let longitude = Double(meteor.reclong)
        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: latitude!)!, longitude: CLLocationDegrees(exactly: longitude!)!)
        let pin = MKPointAnnotation()
        pin.coordinate = coordinates
        mapView.setCenter(coordinates, animated: true)
        mapView.addAnnotation(pin)
        
        massLabel.text = meteor.mass + " \(L10n.MeteorDetails.Labels.massUnit)"
    
        if let yearFormatted = DateHandler().convertToDate(dateString: meteor.year, format: Constants.jsonDateFormat) {
            yearLabel.text = DateHandler().getDateSpecificValue(date: yearFormatted, mode: .year)
        }
        
        classificationLabel.text = L10n.MeteorDetails.Labels.classification + " \(meteor.recclass)"
        latitudeLabel.text = L10n.MeteorDetails.Labels.latitude + " \(meteor.reclat)"
        longitudeLabel.text = L10n.MeteorDetails.Labels.longitude + " \(meteor.reclong)"
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)) { [weak self] (placemarks, error) in
            guard let placemark = placemarks?.first, let isoCode = placemark.isoCountryCode else {
                self?.countryLabel.text = L10n.MeteorDetails.Labels.countryNotFound
                return
            }
            self?.countryLabel.text = CountryFormatter().getNameFrom(iso: isoCode)
        }
    }
    
    private func createDotIcon(imageView: UIImageView) {
        imageView.layer.masksToBounds = false
        imageView.layer.cornerRadius = imageView.frame.height / 2
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.borderWidth = 1.5
    }
    
    @IBAction func openSearchWebView(_ sender: Any) {
        guard let name = meteor?.name else { return }
        let webView = CustomWebViewController()
        webView.load(urlString: GoogleSearchQueryFormatter().formatToGoogleQuery(text: name))
        navigationController?.show(webView, sender: self)
    }
    
    @IBAction func toggleStackView(_ sender: Any) {
        if detailsStackView.isHidden {
            UIView.animate(withDuration: 0.5, animations: {
                self.detailsStackView.isHidden = false
                self.detailsStackView.alpha = 1
                self.dropdownArrow.transform = CGAffineTransform(rotationAngle: 3.14 * 2)
            })
        } else {
            UIView.animate(withDuration: 0.75, animations: {
                self.detailsStackView.alpha = 0
                self.dropdownArrow.transform = CGAffineTransform(rotationAngle: -3.14 / 2)
            }, completion: { _ in
                self.detailsStackView.isHidden = true
            })
        }
    }
}
