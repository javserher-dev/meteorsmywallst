//
//  MeteorsTableViewController.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import RealmSwift

final class MeteorsTableViewController: UITableViewController {
    
    private var meteors = Meteors()
    private var filteredMeteors = Meteors()
    private var isFirstRun = false
    private var isBlurrDisplayed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isFirstRun = UserDefaults.standard.value(forKey: Constants.UserDefaultsKeys.isFirstRun) as? Bool ?? true
        
        setupObservers()
        setupTableView()
        setStyle()
        loadList()
        if !isFirstRun {
            checkForUpdates()
            Timer(timeInterval: 1800, target: self, selector: #selector(checkForUpdates), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(checkForUpdates), name: NSNotification.Name(Constants.NotificationNames.appBecameActive), object: nil)
    }
    
    private func loadList() {
        if isFirstRun {
            let blurrVC = BlurredLoadingScreenViewController()
            blurrVC.startLoading()
            blurrVC.modalPresentationStyle = .overCurrentContext
            present(blurrVC, animated: true, completion: nil)
            isBlurrDisplayed = true
            
            loadRemoteList()
        } else {
            loadLocalList()
        }
    }

    private func setupTableView() {
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 44))
        searchBar.delegate = self
        searchBar.placeholder = L10n.MeteorList.Placeholders.searchBar
        tableView.tableHeaderView = searchBar
        
        tableView.register(UINib(nibName: Constants.Cell.meteorCellNib, bundle: nil), forCellReuseIdentifier: Constants.Cell.cellIdentifier)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(checkForUpdates), for: .allEvents)
        tableView.refreshControl = refreshControl
    }
    
    private func setStyle() {
        navigationController?.navigationBar.topItem?.title = L10n.NavigationBar.Titles.meteorList
        
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 35, bottom: 0, right: 35)
        tableView.separatorColor = .lightGray
        
        if let searchBar = tableView.tableHeaderView as? UISearchBar {
            searchBar.barTintColor = .white
        }
    }
    
    @objc private func checkForUpdates() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            APIClient.shared.loadResponse(router: APIRouter.checkUpdates, completion: { [weak self] response in
                guard let weakSelf = self, let lastModifiedField = response?.allHeaderFields[Constants.APIResponseParameters.lastModified] as? String else { return }
                
                let lastModified = DateHandler().convertToDate(dateString: lastModifiedField, format: Constants.APIResponseParameters.dateFormat)
                let lastRequest = UserDefaults.standard.value(forKey: Constants.UserDefaultsKeys.lastRequest)
                
                if let lastModified = lastModified, let lastRequest = lastRequest as? Date, lastModified > lastRequest {
                    weakSelf.loadRemoteList()
                } else {
                    weakSelf.tableView.refreshControl?.endRefreshing()
                }
            })
        }
    }
    
    private func loadLocalList() {
        guard let dataLoaded = DBManager.shared.load(type: Meteor.self), dataLoaded.count > 0 else {
            loadRemoteList()
            return
        }
        meteors = Meteors(dataLoaded)
        filteredMeteors = meteors
        tableView.reloadData()
    }
    
    private func loadRemoteList() {
        APIClient.shared.performRequest(router: APIRouter.landings, completion: { [weak self] (result: Meteors?) in
            guard let weakSelf = self else { return }
            
            guard let result = result else {
                let alert = TwoButtonsAlertViewController(title: L10n.Alerts.Titles.error, message: L10n.Alerts.Message.remoteLoadError, preferredStyle: .alert)
                alert.setupAlert(leftButtonCompletion: { [weak self] action in
                    self?.dismiss(animated: true, completion: nil)
                }, rightButtonCompletion: { [weak self] action in
                    self?.loadRemoteList()
                })
                
                weakSelf.present(alert, animated: true, completion: nil)
                debugPrint("No results available from the request response")
                return
            }
            
            weakSelf.isFirstRun = false
            UserDefaults.standard.set(false, forKey: Constants.UserDefaultsKeys.isFirstRun)
            UserDefaults.standard.set(Date(), forKey: Constants.UserDefaultsKeys.lastRequest)
            DispatchQueue.main.async {
                DBManager.shared.save(data: result)
            }
            weakSelf.meteors = result
            weakSelf.filteredMeteors = weakSelf.meteors
            
            if weakSelf.isBlurrDisplayed {
                weakSelf.presentedViewController?.dismiss(animated: true, completion: nil)
                weakSelf.isBlurrDisplayed = false
            }
            
            weakSelf.tableView.reloadData()
        })
    }
    
    private func filterList(by text: String) {
        if text.isEmpty {
            filteredMeteors = meteors
        } else {
            filteredMeteors = meteors.filter({
                $0.name.range(of: text, options: [.diacriticInsensitive, .caseInsensitive]) != nil
            })
        }
        tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredMeteors.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.cellIdentifier) as? MeteorTableViewCell else {
            return UITableViewCell()
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? MeteorTableViewCell else { return }
        
        cell.meteor = filteredMeteors[indexPath.row]
        cell.setContent()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC = MeteorDetailsViewController()
        detailsVC.meteor = meteors[indexPath.row]
        show(detailsVC, sender: self)
        tableView.cellForRow(at: indexPath)?.isSelected = false
    }
}

extension MeteorsTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterList(by: searchText)
    }
}
