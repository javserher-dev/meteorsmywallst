//
//  MeteorTableViewCell.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

class MeteorTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    var meteor: Meteor?
    
    func setContent() {
        guard let meteor = meteor, let idInt = Int(meteor.id) else { return }
        imgView.image = Constants.meteorImages[idInt % Constants.meteorImages.count].image
        nameLabel.text = meteor.name
        if let yearFormatted = DateHandler().convertToDate(dateString: meteor.year, format: Constants.jsonDateFormat) {
            yearLabel.text = DateHandler().getDateSpecificValue(date: yearFormatted, mode: .year)
        }
        massLabel.text = String.init(format: "%.2f gr", Double(meteor.mass)!)
    }
}
