//
//  TabBarViewController.swift
//  MeteorsMyWallSt
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setStyle()
        setupControllers()
    }
    
    private func setStyle() {
        tabBar.tintColor = .black
    }
    
    private func setupControllers() {
        let firstVC = UINavigationController(rootViewController: MeteorsTableViewController())
        firstVC.tabBarItem = UITabBarItem(title: "List", image: Asset.listIcon.image, selectedImage: nil)
        
        self.viewControllers = [firstVC]
    }
}
