//
//  SimpleOKAlertViewController.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

private class SimpleOKAlertViewController: UIAlertController {
    
    convenience init(title: String, message: String) {
        self.init(title: title, message: message, preferredStyle: .alert)
    }
    
    func setupByDefault() {
        self.addAction(UIAlertAction(title: L10n.Alerts.Buttons.ok, style: .default, handler: { [weak self] action in
            self?.dismiss(animated: true, completion: nil)
        }))
    }
    
    func setupAlert(completion: @escaping AlertActionCompletion) {
        self.addAction(UIAlertAction(title: L10n.Alerts.Buttons.ok, style: .default, handler: completion))
    }
}
