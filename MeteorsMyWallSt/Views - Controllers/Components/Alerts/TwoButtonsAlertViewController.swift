//
//  TwoButtonsAlertViewController.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class TwoButtonsAlertViewController: UIAlertController {
    
    func setupAlert(leftButtonCompletion: @escaping AlertActionCompletion, rightButtonCompletion: @escaping AlertActionCompletion) {
        self.addAction(UIAlertAction(title: L10n.Alerts.Buttons.cancel, style: .default, handler: leftButtonCompletion))
        self.addAction(UIAlertAction(title: L10n.Alerts.Buttons.ok, style: .default, handler: rightButtonCompletion))
    }
}
