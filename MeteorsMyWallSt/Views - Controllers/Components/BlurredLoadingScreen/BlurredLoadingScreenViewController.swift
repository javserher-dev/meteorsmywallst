//
//  BlurredLoadingScreenViewController.swift
//  BarcodeDetector
//
//  Created by Javier Servate on 22/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class BlurredLoadingScreenViewController: UIViewController {
    
    var loadingSpinner = UIActivityIndicatorView()
    var loadingLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setStyle()
    }
    
    private func setupView() {
        let blurr = UIBlurEffect(style: .light)
        let blurrView = UIVisualEffectView(effect: blurr)
        blurrView.frame = view.bounds
        view.addSubview(blurrView)
        
        loadingSpinner.frame = CGRect(x: 0, y: 0, width: 75, height: 75)
        loadingSpinner.center = view.center
        view.addSubview(loadingSpinner)
        
        let centerOffset = loadingSpinner.frame.height + 10
        loadingLabel.frame = CGRect(x: 0, y: 0, width: 250, height: 75)
        loadingLabel.center = CGPoint(x: view.center.x, y: view.center.y - centerOffset)
        loadingLabel.text = L10n.BlurredLoadingScreen.Labels.loading
        view.addSubview(loadingLabel)
    }
    
    private func setStyle() {
        loadingSpinner.color = .black
        loadingLabel.textColor = .black
        loadingLabel.font = loadingLabel.font.withSize(22)
    }
    
    func startLoading() {
        loadingSpinner.startAnimating()
    }
    
    func stopLoading() {
        loadingSpinner.stopAnimating()
        dismiss(animated: true, completion: nil)
    }
}
